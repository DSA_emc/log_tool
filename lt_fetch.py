# ************************************************ <-- 125 characters -> ***************************************************
#
# DDDD     SSSS    AAA        
# D   D   SS      A   A       
# D   D    SSS    AAAAA       
# D   D      SS   A   A       
# DDDD    SSSS    A   A       
# 
# **************************************************************************************************************************
# 
# $Source: lt_fetch.py  $                 $Author: emc $                  $Revision:  $                 $Date: 31/07/2020 $
# 
# **************************************************************************************************************************
import os
import collections
import time
import json
import argparse
import logging
import logging.config
import concurrent.futures
import yaml
import sys
import datetime
from dsa.mx import config
from dsa.mx.lt.devices import DSADevice
import dsa.mx.payload as payload
import dsa.mx.lib.utils as dsautils
############################################################################################################################
### Global inits
############################################################################################################################

### logging
with open('resources/logging.conf', 'r') as f:
    log_cfg = yaml.safe_load(f.read())
logging.config.dictConfig(log_cfg)

logger = logging.getLogger('dsa.mx.logtool.lft_fetch')

### set up execution time
str_date = datetime.datetime.today().strftime("%Y-%m-%d_%H.%M.%S")
config.addParam("execution_time_str", str_date)

# --------------------------------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------------------------------
### Parsing command line arguments
parser = argparse.ArgumentParser(description='Tool that aims to gather logs and health information from DSA-devices')
# dir to process
parser.add_argument('--devices',       "-d",  metavar='list', type=str, required=True, nargs="+",
                    help=" ".join(("List of target devices. Allows a list",
                      "of IPs or device-names ('device-config' option needs to be provided).",
                      "If the IP for a target hostname is not found in 'device-config' file, then is calculated from",
                      " hostname according to VWM rules, ex: MFT3-256 = 10.148.47.1")))
parser.add_argument('--device-config', "-dc", metavar='filepath', type=str,
                   help='Filepath of a device config list that contains the information (IPs)')
parser.add_argument('--output_dir',    "-o",  metavar='filepath', type=str, 
                    default=config.getParam("general.default_output_dir"),
                    help="Directory where the output will be placed. Default dir: {}"
                      .format(config.getParam("general.default_output_dir")))
parser.add_argument('--tasks',    "-t",  metavar='task_list', type=str, nargs="*",
                   choices=['logs', 'status', 'cleanup'],
                   help="List of tasks to be performed. Allowed values: logs, status, cleanup.")
parser.add_argument('--payload',    "-p",  metavar='payload_list', type=str, nargs="*",
                   help=" ".join(("List of payloads to be delivered and executed in the target MFT.",
                    "Payload files are taken from '{}' directory".format(config.getParam("general.payload_dir")))))
parser.add_argument('--note',    "-n",  metavar='note', type=str, default="-",
                   help="Add a note to describe the purpose of this task")
############################################################################################################################
### Parsing & validating & processing command line arguments
############################################################################################################################
args = parser.parse_args()
logger.info("")
logger.info(args.note)
logger.info("**************************************************************************************************************************")
logger.info("************************************************ DSA log tool ************************************************************")
logger.info("**************************************************************************************************************************")
logger.info("")

output_path = args.output_dir
device_list = args.devices
device_config = args.device_config

tasks = [] if args.tasks == None else args.tasks

payload_content = None
payload = [] if args.payload == None else args.payload
if(args.payload):
  payload_content = payload.load(args.payload, config.getParam("general.payload_dir"))

# validate if device_config file exists
if(device_config is not None):
  if(not os.path.exists(device_config)):
    raise Exception("Device config file doesn't exist {}".format(device_config))

  deviceDict= json.load(open(device_config))
  device_list = dsautils.resolveDevices(deviceDict, device_list)
else:
  device_list = [ (device, device) for device in device_list]

deviceProcessInfo = collections.OrderedDict()
for device in device_list:
  deviceProcessInfo[device[0]] = {"IP:": device[1], "Status": "NOT"}

############################################################################################################################
### Validations
############################################################################################################################
if(len(device_list)==0):
  logger.error("There are no devices to be processed")
  sys.exit()

### payload argument validation
if(args.payload and not payload_content):
    raise Exception("Provided payload files does not contain information")

### If there is no tasks or payload to deliver, then cancel process
if(not payload_content and not tasks): 
  logger.error("At least one goal(task or payload) is expected. No actions to be performed... Exit")
  sys.exit()
  
############################################################################################################################
### Processing
############################################################################################################################
def threadx(device_info):
  logger.info("Processing {}".format(device_info))

  device = DSADevice(device_info[0], device_info[1], 
    config.getParam("general.user"), config.getParam("general.password"))
  try:
    device.openConnection()
  except Exception as e:
    deviceProcessInfo[device_info[0]]["status"] ="NOK"
    logger.error("Unable to connect with {}".format(device))  
    logger.debug("Exception when connectiong to {}".format(device_info), exc_info=e)
    return
  deviceProcessInfo[device_info[0]]["status"] ="OK"
  ### processing user tasks
  if("logs" in tasks):
    device.gatherLogs()
    device.gatherStatus()
  elif("status" in tasks):
    device.gatherStatus()
  ### if there is a payload indication
  if(payload_content):
    device.addPayload(payload_content)
    device.deliverPayload()
  device.fetchData(output_path)

  # clear remote informatio based on configuration
  if(config.getParam("general.cleanAfter")):
    device.clean()

### this structure will hold the results of the tests
deviceProcessInfo = collections.OrderedDict()
for device in device_list:
  deviceProcessInfo[device[0]] = {"IP": device[1], "status": "NOT"}
############################################################################################################################
### Processing
############################################################################################################################
threads = list()

nMaxThreads = config.getParam("general.parallel.nMaxThreads")
# Parallel threas by MFT
logger.debug("ThreadPoolExecutor. Max parallel threads {}".format(nMaxThreads))
with concurrent.futures.ThreadPoolExecutor(max_workers=nMaxThreads) as executor:
    executor.map(threadx, device_list)

### print summary table
logger.info("")
logger.info("Summary table:")
logger.info("")
logger.info("-"*48)
layout_summary = "|{:>20}|{:>16}|{:>8}|"
# headers
logger.info(layout_summary.format("Device name    ", "Device IP   ", "Status"))
logger.info("-"*48)
for deviceInfoKey in deviceProcessInfo:
  deviceInfo = deviceProcessInfo[deviceInfoKey]
  logger.info(layout_summary.format(deviceInfoKey,  deviceInfo["IP"], deviceInfo["status"]))
logger.info("-"*48)

metadata_file = os.path.join(output_path, "info.csv")
if(not os.path.exists(metadata_file)):
  file = open(metadata_file, "w")
  file.write("tool,Notes,Execution time,Description,Targets\n")
else:
  file = open(metadata_file, 'a')
description = "tasks:[{}]-payload:[{}]".format(";".join(tasks),";".join(payload))
file.write("{},{},{},{},{}\n".format("lt_fetch", args.note,str_date,description, ";".join([a[0] for a in device_list])))
file.close()