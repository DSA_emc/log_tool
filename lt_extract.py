# ************************************************ <-- 125 characters -> ***************************************************
#
# DDDD     SSSS    AAA        
# D   D   SS      A   A       
# D   D    SSS    AAAAA       
# D   D      SS   A   A       
# DDDD    SSSS    A   A       
# 
# **************************************************************************************************************************
# 
# $Source: lt_extract.py  $                 $Author: emc $                  $Revision:  $                 $Date: 31/07/2020 $
# 
# **************************************************************************************************************************
# Integrate exported files
# **************************************************************************************************************************
import os
import collections
import time
import json
import argparse
import dsa.mx.lib.utils as dsautils
import logging
import logging.config
import concurrent.futures
import pprint
import yaml
import sys
import datetime
import dsa.mx.lib.tasks as tasks
from dsa.mx import config

############################################################################################################################
### Global inits
############################################################################################################################
### logging 
with open('resources/logging.conf', 'r') as f: log_cfg = yaml.safe_load(f.read())
logging.config.dictConfig(log_cfg)
logger = logging.getLogger('dsa.mx.logtool.lft_fetch')

### set up execution time
str_date = datetime.datetime.today().strftime("%Y-%m-%d_%H.%M.%S")
config.addParam("execution_time_str", str_date)
def task_runner(device_info, task, params):
  task_function = getattr(tasks, task)
  task_function(device_info, params)

def threadx(device_info, tasks_list, params):
  try:
    prefix = "[{:>12}][{}]".format(device_info[0], device_info[1][device_info[1].find("_")+1:])
    logger.info("{} Processing {}".format(prefix, device_info[1]))
    for task in tasks_list:
      logger.debug("{} Performing task {}".format(prefix,task))
      task_runner(device_info, task, params)
  except:
    logger.error("", stack_info=True, exc_info=True)

def gatherDevices(workspace_dir):
  devices = collections.OrderedDict()
  logger.debug("Looking for entries in {} with option '{}'...".format(workspace_dir, process_entries))
  device_list = []
  for device_entry in os.listdir(workspace_dir):
    device_names = config.getParam("extract.device_names")
    if(not (device_entry.startswith(tuple(device_names)))):
      logger.debug("Ignoring entry {}".format(device_entry))
      continue
    logger.debug("Looking into {}".format(device_entry))
    for dir_entry in os.listdir(os.path.join(workspace_dir, device_entry)):
      entry_path = os.path.join(workspace_dir, device_entry, dir_entry)
      if(not os.path.isdir(entry_path)):
        logger.debug("  Files are ignored {}".format(dir_entry))
        continue
      # all testers need to end with predefined timeformat
      time_part = dir_entry[dir_entry.find("_")+1:]
      try:
        entry_date = datetime.datetime.strptime(time_part, time_format)     
      except:
        logger.warning("  A problem occurred when processing entry {}".format(entry_path))
        logger.debug("  {} ".format(entry_path), stack_info=True)
        continue
      if(device_entry not in devices):
        logger.debug("First entry found {}".format(dir_entry))
        devices[device_entry] = {"entry": dir_entry, "path": entry_path, "time": entry_date}

      if(process_entries == "latest_timestamp"):
        if(entry_date > devices[device_entry]["time"]):
          logger.debug("Newest device entry found {}".format(dir_entry))
          devices[device_entry]["entry"] = dir_entry
          devices[device_entry]["path"] = entry_path
      else:
        logger.info("  Entry {} of device {} added to device list ".format(dir_entry, device_entry))
        device_list.append((device_entry, dir_entry, entry_path))
  if(process_entries == "latest_timestamp"):
    device_list = [(entry, devices[entry]["entry"], devices[entry]["path"])  for entry in devices.keys()]

  return device_list

if __name__ == '__main__':
  tasks_config = config.getParam("extract.tasks")

  ### extracting information from tasks configuration to getting cli ready
  task_list = []
  extra_params = collections.OrderedDict()
  for task in tasks_config:
    task_list.append(task)
    task_conf = tasks_config[task]
    if("parameters" not in task_conf): continue
    for sparam in task_conf["parameters"]:
      params_conf = task_conf["parameters"]
      # if is a comment
      if(sparam.startswith("__") and sparam.endswith("__")):
        comment = params_conf[sparam]
        param_name = "{}_{}".format(task,sparam[2:-2])
        extra_params[param_name] = comment
  extended_task_list = ["all"]+task_list
  # --------------------------------------------------------------------------------------------------------------------------
  # --------------------------------------------------------------------------------------------------------------------------
  ### Parsing command line arguments
  parser = argparse.ArgumentParser(description='Tool to summarize information from data gathered by lt_fetch.py')
  # dir to process
  parser.add_argument('--workspace_dir',   "-w",  metavar='filepath', type=str, 
                    default=config.getParam("general.default_output_dir"),
                    help=("Directory where the working data is stored"))
  parser.add_argument('--devices',         "-d",  metavar='list', type=str,
                      help=" ".join(("List of devices from where the data will be extracted. Allows a comma separated list.",
                      "If not provided all the folder with format MFT3.*, MDIHOST.* are processed")))
  parser.add_argument('--tasks',    "-t",  metavar='task', type=str, required=True,
                    nargs="+", choices=extended_task_list,
                    help=" ".join(("List of tasks to be performed. Tasks are defined/configured in",
                      "config.json/extract/tasks. Allowed tasks are: {}".format(extended_task_list))))
  parser.add_argument('--process_entries',"-pe",  metavar='option', type=str,  default='latest_timestamp',
                    choices=['all','latest_timestamp'],
                    help=" ".join(("If 'all' all entries found per device folder are processed;",
                      "with 'latest_timestamp' only the folder with the lastest timestamp is processed")))
  parser.add_argument('--note',    "-n",  metavar='note', type=str, default="-",
                   help="Add a note to describe the purpose of this task")
  ### adding parameters from configuration
  for param in extra_params:
    parser.add_argument("--{}".format(param),  type=str, help=extra_params[param])

  ############################################################################################################################
  ### Parsing & validating & processing command line arguments
  ############################################################################################################################
  args = parser.parse_args()
  logger.info("")
  logger.info("**************************************************************************************************************************")
  logger.info("************************************************ DSA log tool ************************************************************")
  logger.info("**************************************************************************************************************************")
  logger.info("")

  workspace_dir = args.workspace_dir
  devices = args.devices
  target_tasks = args.tasks
  process_entries = args.process_entries

  ### validate dynamic generated arguments
  params = {}
  logger.debug("Getting tasks in dependency order...")
  target_tasks = None if ("all" in target_tasks) else target_tasks
  task_order = dsautils.task_order(tasks_config, target_tasks=target_tasks)

  for task in task_order:
    task_config = tasks_config[task]
    for arg_name in args.__dict__:
      if(not arg_name.startswith(task)): continue
      value_arg =getattr(args, arg_name)
      param = arg_name[len(task)+1:]
      if(value_arg == None and param not in task_config["parameters"]):
        logger.fatal("Argparse: '{}' for task '{}' must be provided".format(arg_name, task))
        sys.exit()
      if(task not in params): params[task] = {}
      params[task][param] = value_arg if (value_arg != None) else task_config["parameters"][param]

  ############################################################################################################################
  ### Processing
  ############################################################################################################################
  ### this structure will hold the results of the tests
  deviceProcessInfo = collections.OrderedDict()

  ############################################################################################################################
  ### Globals
  ############################################################################################################################
  time_format = "%Y-%m-%d_%H.%M.%S"

  ############################################################################################################################
  ### Processing
  ############################################################################################################################
  ### gather device list 
  device_list = gatherDevices(workspace_dir)

  if(len(device_list)==0):
    logger.error("There are no devices to be processed")
    sys.exit()

  nMaxThreads = config.getParam("general.parallel.nMaxThreads")
  import time

  start = time.time()
  # Parallel threas by MFT
  logger.debug("ThreadPoolExecutor. Max parallel threads {}, devices: {}".format(nMaxThreads, len(device_list)))
  ttt = [task_order for device in device_list]
  ppp = [params for device in device_list]
  with concurrent.futures.ProcessPoolExecutor(max_workers=nMaxThreads) as executor:
    #futures = {executor.submit(device, task_order) for device in device_list}
    executor.map(threadx, device_list, ttt, ppp)
  end = time.time()
  logger.info("")
  logger.info("Total elapsed time: {}".format(end - start))
  '''
  ### print summary table
  logger.info("")
  logger.info("Summary table:")
  logger.info("")
  logger.info("-"*48)
  layout_summary = "|{:>20}|{:>16}|{:>8}|"
  # headers
  logger.info(layout_summary.format("Device name    ", "Device IP   ", "Status"))
  logger.info("-"*48)
  for deviceInfoKey in deviceProcessInfo:
    deviceInfo = deviceProcessInfo[deviceInfoKey]
    logger.info(layout_summary.format(deviceInfoKey,  deviceInfo["IP"], deviceInfo["status"]))
  logger.info("-"*48)
  '''
  logger.info("")
  logger.info("******************************************** END DSA log tool ************************************************************")
  logger.info("**************************************************************************************************************************")
  logger.info("")

  metadata_file = os.path.join(workspace_dir, "info.csv")
  if(not os.path.exists(metadata_file)):
    file = open(metadata_file, "w")
    file.write("tool,Notes,Execution time,Description,Targets\n")
  else:
    file = open(metadata_file, 'a')
  description = "tasks:[{}]".format(";".join(task_order))
  file.write("{},{},{},{},{}\n".format("lt_fetch", args.note,str_date,description, ";".join([a[0] for a in device_list])))
  file.close()