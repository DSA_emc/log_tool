import json
import logging
import pprint

config_filepath = "resources/config.json"
configuration = None
logger = logging.getLogger(__name__)

def getConfig():
    logger.info("Retrieving configuration from {}".format(config_filepath))
    global configuration
    try:
        configuration = json.load(open(config_filepath))
        logger.debug(pprint.pformat(configuration, indent=2, width=122, depth=None))
    except:
        logger.error("Could not load configuration from '{}'".format(config_filepath), stack_info=True)
    return configuration

def getParam(parameter):
    global configuration
    logger.info("Retrieving parameter '{}'".format(parameter))
    if(configuration == None):
        logger.info("Configuration has not been initialized...")
        configuration = getConfig()
    value_setting = configuration
    for key in parameter.split("."):
        if(key in value_setting):
            value_setting = value_setting[key]
        else:
            logger.error("The parameter {} does not exist".format(parameter), stack_info=True)
            raise Exception("The parameter {} does not exist".format(parameter))
    return value_setting

def addParam(parameter, value):
    global configuration
    logger.info("Adding parameter parameter '{}'".format(parameter))
    if(configuration == None):
        logger.info("Configuration has not been initialized...")
        configuration = getConfig()
    value_setting = configuration
    pstring = ""
    for key in parameter.split("."):
        pstring = key if(pstring == "") else "{}.{}".format(pstring, key)
        # overwrite parameter
        if(pstring == parameter):
            logger.info("Overwriting existing parameter {}".format(parameter))
            value_setting[key] = value
            break
        else:
            if(key not in value_setting):
                logger.info("Creating entry parameter parameter {}".format(pstring))
                value_setting[key] = dict()
            value_setting = value_setting[key]