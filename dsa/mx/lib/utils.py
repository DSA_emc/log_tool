# ************************************************ <-- 125 characters -> ***************************************************
#
# DDDD     SSSS    AAA        
# D   D   SS      A   A       
# D   D    SSS    AAAAA       
# D   D      SS   A   A       
# DDDD    SSSS    A   A       
# 
# **************************************************************************************************************************
# 
# $Source: import.py  $                   $Author: emc $                  $Revision:  $                 $Date: 17/05/2018 $
# 
# **************************************************************************************************************************
# Integrate exported files
# **************************************************************************************************************************
import os
import sys
import logging
import gzip
import shutil
import tarfile
import re

logger = logging.getLogger('dsa.mx.lib')

def getScriptDir():
    (script_path, _) = os.path.split(sys.argv[0])             # to kept execution relative to script location
    if(script_path == ''):
        (script_path, _) = os.path.split(sys.executable)      # to kept execution relative to script location}    
    return script_path
    
def tgzExtract(tarfile_path, target_source):
  logger.info("Extracting {} to {}...".format(tarfile_path, target_source))
  if(not os.path.exists(target_source)):
    logger.info("Creating dir {}".format(target_source))
    os.makedirs(target_source)
  file = tarfile.open(tarfile_path, "r:gz")
  file.extractall(path=target_source)
  file.close()
  
def gzExtract(gz_path, target_source, dir_output=False):
  logger.info("Extracting {} to {}...".format(gz_path, target_source))
  if(dir_output and not os.path.exists(target_source)):
    logger.info("Creating dir {}".format(target_source))
    os.makedirs(target_source)

  with gzip.open(gz_path, 'rb') as f_in:
      with open(target_source, 'wb') as f_out:
          shutil.copyfileobj(f_in, f_out)
          
############################################################################################################################
def resolveDevices(deviceDict, entryPoints):
  if(isinstance(entryPoints, str)):
    comp_expression = re.compile("^{}$".format(entryPoints))
    
    entrys = []
    for deviceName in deviceDict.keys():
      if(comp_expression.match(deviceName)):
        entrys.append((deviceName, deviceDict[deviceName]))
        logger.info("Entry found {}".format(deviceName))
    logger.debug("{} entries found".format(len(entrys)))

    # base case
    device_list = []
    # no entries
    if(len(entrys) == 0):
      if(entryPoints.startswith("MFT3-") or entryPoints.startswith("MDIHOST-")):
        tup = (entryPoints, "DEFAULT_IP")
        logger.info("Device with default IP: {}".format(tup))
        device_list.append(tup)
    else:
      for entry in entrys:
        if(isinstance(entry[1], str)):
          tup = (entry[0], entry[1])
          logger.info("Device found: {}".format(tup))
          device_list.append(tup)
        else:
          for e in entry[1]:
            device_list = device_list + resolveDevices(deviceDict, e)
    
    return device_list
  elif(isinstance(entryPoints, list)):
    deviceList = []
    for entryPoint in entryPoints:
      deviceList = deviceList + resolveDevices(deviceDict, entryPoint)
  else:
    raise Exception("Unexpected argument type")
  return deviceList 

def task_order(tasks_config, order=[], target_tasks=None):
  if(target_tasks == None): 
    logger.info("Entry point, no target tasks")
    target_tasks = tasks_config.keys()

  for task in target_tasks:
    task_conf = tasks_config[task]
    if(task in order): 
      logger.debug("Task '{}' already in result list".format(task))
      continue

    if("preconditions" in task_conf):
      logger.debug("Task '{}', recursive case, preconditions found".format(task))
      order = task_order(tasks_config, order=order, target_tasks=task_conf["preconditions"]) + [task]
    else:
      logger.debug("Task '{}', base case, new task added".format(task))
      order = order + [task]
  return order