# ************************************************ <-- 125 characters -> ***************************************************
#
# DDDD     SSSS    AAA        
# D   D   SS      A   A       
# D   D    SSS    AAAAA       
# D   D      SS   A   A       
# DDDD    SSSS    A   A       
# 
# **************************************************************************************************************************
# 
# $Source: tasks.py  $                   $Author: emc $                  $Revision:  $                 $Date: 31/07/2020 $
# 
# **************************************************************************************************************************
# Repository of common functions
# **************************************************************************************************************************
import shutil
import re
import os
import pysftp
import tarfile
import gzip
from timeit import default_timer as timer
import time
import json
import logging
import dsa.mx.lib.utils as utils
import fileinput
from time import strptime

logger = logging.getLogger('dsa.mx.logtool.tasks')

def unzip_logstatus(device_info, params):
  prefix = "[{:>12}][{}]".format(device_info[0], device_info[1][device_info[1].find("_")+1:])
  task__name = "unzip_logstatus"
  working_dir = device_info[2]
  for entry in os.listdir(working_dir):
    entry_path = os.path.join(working_dir, entry)
    if(os.path.isdir(entry_path) or not entry.endswith(".tgz")): continue
    logger.info("{} Unzipping logstatus file {}".format(prefix, entry))
    logstatus_out_dir = os.path.join(working_dir, entry[:entry.rfind(".")])
    params[task__name]["unzipped_dir"] = logstatus_out_dir
    if(os.path.exists(logstatus_out_dir)):
      logger.warning("{} Directory {} already exist. Skipping function".format(prefix, logstatus_out_dir))
      continue

    utils.tgzExtract(entry_path, logstatus_out_dir)
    for (base_path, _, files) in os.walk(os.path.join(logstatus_out_dir, "var/opt/dsa/log")):
      for entry_file in files:
        if(not entry_file.endswith(".gz")): continue
        filepath = os.path.join(base_path, entry_file)
        filepath_out = os.path.join(base_path, entry_file[:entry_file.rfind(".")])
        utils.gzExtract(filepath, filepath_out)
        os.remove(filepath)  
    break

def time_screening(device_info, params):
  prefix = "[{:>12}][{}]".format(device_info[0], device_info[1][device_info[1].find("_")+1:])
  log_path = os.path.join(params["unzip_logstatus"]["unzipped_dir"], "var/opt/dsa/log")
  t_pat = re.compile(r'(\d{6} \d{6}\.\d{6})')
  t_fmt_infile = r"%y%m%d %H%M%S.%f"
  
  ### validating timestamps parameters
  t_fmt = params["time_screening"]["format"]
  boundary_start_time_str = params["time_screening"]["start"]
  boundary_end_time_str = params["time_screening"]["end"]
  logger.info("{} Retrieving logs between [{}-{}]".format(prefix, boundary_start_time_str, boundary_end_time_str))
  boundary_start_time = strptime(boundary_start_time_str, t_fmt)
  boundary_end_time = strptime(boundary_end_time_str, t_fmt)
  of = os.path.join(device_info[2], "time_screening")
  logger.debug("{} Screening output file {}".format(prefix, of))
  logger.debug("{} Logpath: {} ".format(prefix, log_path))
  of_file = open(of,"w", encoding='utf8')
  f_names = set()
  for (base_path, _, files) in os.walk(log_path):
    for file_entry in files:
      filepath = os.path.join(base_path, file_entry)
      f_names.add(filepath)

  lines = []
  for line in fileinput.FileInput(list(f_names), openhook=fileinput.hook_encoded("utf-8", errors="replace")):
    line = line.strip()
    match = t_pat.search(line)
    if(match):
      ctime = strptime(t_pat.search(line).group(1), t_fmt_infile)
      if(boundary_start_time <= ctime and ctime <= boundary_end_time):
        lines.append(u"{}\n".format(line))
        #of_file.write(u"{}\n".format(line))
  # sort lines by timestamp
  lines = sorted(lines, key=lambda l: strptime(t_pat.search(l).group(1), t_fmt_infile))
  of_file.writelines(lines)
  of_file.close()