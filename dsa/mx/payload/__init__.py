import logging
import os

logger = logging.getLogger(__name__)

def load(payload_list, directory):
    logger.info("Payload targets: {} from {}".format(payload_list, directory))
    payload = []
    for payload_file in payload_list:
        payload_filepath = os.path.join(directory, payload_file)
        payload = payload + readPayload(payload_filepath)
    return payload    

def readPayload(payload_filepath):    
    # validate if payload exist
    if(not os.path.exists(payload_filepath)):
        message = "Payload file {} not exist. Please verify it and try again".format(payload_filepath)
        logger.error(message)
        raise Exception(message)
    payload = []
    logger.debug("Validating payload file '{}'".format(payload_filepath))
    pl_file = open(payload_filepath)
    for line in pl_file.readlines():
        line = line.strip()
        if(line.startswith("#") or line == ""): continue
        payload.append(line)
        logger.debug("Adding to payload line {} ".format(line))
    return payload