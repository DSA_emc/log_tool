# ************************************************ <-- 125 characters -> ***************************************************
#
# DDDD     SSSS    AAA        
# D   D   SS      A   A       
# D   D    SSS    AAAAA       
# D   D      SS   A   A       
# DDDD    SSSS    A   A       
# 
# **************************************************************************************************************************
# 
# $Source: commons.py  $                   $Author: emc $                  $Revision:  $                 $Date: 31/07/2020 $
# 
# **************************************************************************************************************************
# Repository of common functions
# **************************************************************************************************************************
import os
import pysftp
import json
import datetime
import logging
import dsa.mx.payload as payload
from dsa.mx import config

logger = logging.getLogger('dsa.mx.logtool')
############################################################################################################################
class DSADevice:
  def __init__(self, name, ip, username, password):
    if(ip == "DEFAULT_IP"):
      logger.info("Calculating IP from hostname")
      last_ip_value = int(name[name.find("-")+1:])
      ip = "10.148.46.{}".format(last_ip_value) if last_ip_value <= 255 else "10.148.47.{}".format(last_ip_value-255)
      logger.info("IP from hostname {}".format(ip))
    self.name = name
    self.ip = ip
    self.username = username
    self.password = password
    logger.debug("Creating DSADevice: {},{}".format(self.name,self.ip))
    self.payload = list()
    self.message_prefix = "[{}]".format(self.name)
  
  def openConnection(self):
    message_prefix = "[{}]".format(self.name)

    cnopts = pysftp.CnOpts()
    cnopts.hostkeys = None 
    logger.info("{}. {}".format(message_prefix, "Trying connection"))
    self.sftp = pysftp.Connection(host=self.ip, username=self.username, password=self.password, cnopts=cnopts)
    self.remoteFileDir = config.getParam("execution_time_str")
    self.remoteFilepath = "/tmp/{}_{}".format(self.name, self.remoteFileDir)
    logger.debug("Creating remote directory {}".format(self.remoteFilepath))
    # create result directory
    self.sftp.execute("mkdir -p {}".format(self.remoteFilepath))
    #self.sftp.get_r('/home/tester', os.path.normpath(os.path.relpath('C:\\trash'))) #, preserve_mtime=True)
  
  def gatherLogs(self):
    message_prefix = "[{}]".format(self.name)

    # Run remote commands
    logger.info("{}. {}".format(message_prefix, "Gathering logs"))
    command = "/opt/dsa/tools/bin/logstatus -A"
    logger.debug("{}. Remote command '{}'".format(message_prefix, command))

    results = self.sftp.execute(command)
    
    if(len(results)!=1): 
      raise Exception("Unexpected result for 'logstatus' command: {}".format(results))
    logstatus_filepath = results[0].strip().decode("utf-8")

    logger.debug("{}. Moving results from {} to {}".format(message_prefix, logstatus_filepath, self.remoteFilepath))
    mv_command = "mv {} {}".format(logstatus_filepath, self.remoteFilepath)
    logger.debug("{}. Remote command '{}'".format(message_prefix, mv_command))
    self.sftp.execute(mv_command) 

  def gatherStatus(self):
    message_prefix = "[{}]".format(self.name)
    # Run remote commands
    logger.info("{}. {}".format(message_prefix, "Health status"))
    status_payload = payload.load(["device_status"], config.getParam("general.payload_dir"))
    self.__deliverPayload(status_payload)

  def addPayload(self, payload):
    if(type(payload) == list):
      logger.debug("{}. Adding list of payloads {}".format(self.message_prefix, payload))
      self.payload = self.payload + payload
    else:
      logger.debug("{}. Adding single entry payload {}".format(self.message_prefix, payload))
      self.payload.append(payload)

  def __deliverPayload(self, payload):
    logger.info("{}. Delivering payload...".format(self.message_prefix))
    for payload_entry in payload:
      command = payload_entry.format(self.remoteFilepath)
      logger.debug("{}. Delivering -> '{}'".format(self.message_prefix, command))
      results = self.sftp.execute(command)
      logger.debug("{}. Results: {}".format(self.message_prefix, results))

  def deliverPayload(self):
    self.__deliverPayload(self.payload)

  def fetchData(self, result_path):
    message_prefix = "[{}]".format(self.name)

    result_path = os.path.join(result_path, self.name, "{}_{}".format(self.name, self.remoteFileDir))
    if(not os.path.exists(result_path)): os.makedirs(result_path)

    # Run remote commands
    logger.info("{}. {}".format(message_prefix, "Fetching data"))

    logger.info("{}. Transfering results from remote {} to local {}".format(message_prefix, self.remoteFilepath, result_path))
      
    self.sftp.get_d(self.remoteFilepath, result_path, preserve_mtime=True)
    #self.get_remote_dir_recursive('/home/tester','/home/tester', result_path) #, preserve_mtime=True)

  def get_remote_dir_recursive(self, base_dir, remote_dir, local_dir, preserve_mtime=True):
    for entry in os.listdir(remote_dir):
      tmp_path = os.path.join(remote_dir, entry)
      print("++"+tmp_path)
      if(os.path.isdir(tmp_path)):
        #get_remote_dir_recursive(tmp_path)
        print("found entry "+tmp_path)
        print("found relpa "+os.path.relpath(tmp_path, base_dir))
        self.get_remote_dir_recursive(base_dir, tmp_path, local_dir)
      #else:
      #  self.sftp.get(tmp_path, )

  def clean(self):
    message_prefix = "[{}]".format(self.name)
    # Run remote commands
    logger.info("{}. Cleaning remote directory {}".format(message_prefix, self.remoteFilepath))
    # lock to just clean information in tmp foldre
    if(not self.remoteFilepath.startswith("/tmp/")):
      logger.warning("Clean operation is only allowed in tmp folder (current {0}). Skipping...".format(
        self.remoteFileDir))
      return
    mv_command = "rm -r {}".format(self.remoteFilepath)
    logger.debug("{}. Remote command '{}'".format(message_prefix, mv_command))
    self.sftp.execute(mv_command) 
  
  def __str__(self):
    return "[{}, IP:{}]".format(self.name, self.ip)